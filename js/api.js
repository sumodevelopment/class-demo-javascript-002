const POSTS_URL = 'https://jsonplaceholder.typicode.com/posts';
const USERS_URL = 'https://jsonplaceholder.typicode.com/users';
const COMMENTS_URL = 'https://jsonplaceholder.typicode.com/comments';

export const getPosts = () => {
    return fetch( POSTS_URL ).then(response => response.json())
};

export const getPost = id => {
    return fetch( `${POSTS_URL}/${id}` ).then(r => r.json())
}

export const getUser = id => {
    return fetch( `${USERS_URL}/${id}` ).then(r => r.json())
}

export const getUsers = () => {
    return fetch( USERS_URL ).then(response => response.json())
};

export const getCommentsForPost = postId => {
    return fetch( `${COMMENTS_URL}?postId=${postId}` ).then(r => r.json())
}
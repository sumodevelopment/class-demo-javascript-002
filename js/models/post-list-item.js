import PostItem from './post-item.js';

class PostListItem extends PostItem {
    constructor(id, title, body, userId) {
        // Invoke the parent class - PostItem
        super(id, title, body, userId);
    }

    render() {
        // Get the elPost Dom object from the Parent class.
        const elPost = super.render();

        // Add the Anchor tag to the Dom Element.
        const elPostLink = document.createElement('a');
        elPostLink.className = 'post-link';
        elPostLink.href = 'post-detail.html?id=' + this.id;
        elPostLink.innerHTML = '<span class="material-icons">visibility</span> View';
        elPost.appendChild(elPostLink);

        return elPost;
    }
}

export default PostListItem;
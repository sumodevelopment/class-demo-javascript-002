class PostItem {
    constructor(id, title, body, userId) {
        this.id = id;
        this.title = title;
        this.body = body;
        this.userId = userId;
    }

    render() {
        const elPost = document.createElement('div');
        elPost.className = 'post';
    
        const elTitle = document.createElement('h4');
        elTitle.className = 'post-title';
        elTitle.innerText = this.title;
        elPost.appendChild( elTitle );
    
        const elBody = document.createElement('p');
        elBody.className = 'post-body';
        elBody.innerText = this.body;
        elPost.appendChild( elBody );        
    
        return elPost;
    }
}

export default PostItem;
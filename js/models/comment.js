class Comment {
    
    constructor(id, name, body, postId) {
        this.id = id;
        this.name = name;
        this.body = body;
        this.postId = postId;
    }

    render() {

        console.log('Render me!');

        const elComment = document.createElement('div');
        elComment.className = 'comment';
    
        const elCommentBody = document.createElement('p');
        elCommentBody.className = 'comment-body';
        elCommentBody.innerText = this.body;
        elComment.appendChild( elCommentBody );
    
        const elCommentUser = document.createElement('small');
        elCommentUser.className = 'comment-user';
        elCommentUser.innerText = this.name;
        elComment.appendChild( elCommentUser );

        
    
        return elComment;
    }
}

export default Comment;
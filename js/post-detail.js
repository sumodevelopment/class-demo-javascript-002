import { getPost, getUser, getCommentsForPost } from './api.js';
import PostItem from './models/post-item.js';
import Comment from './models/comment.js';

class PostDetail {

    constructor() {
        this.elPostContent  = document.getElementById('post-content');
        this.elPostTitle    = document.getElementById('post-title');
        this.elPostBody     = document.getElementById('post-body')
        this.elUser         = document.getElementById('user');
        this.elBtnLoadComments = document.getElementById('btn-load-comments');
        this.elComments = document.getElementById('comments');

        this.post = null;
        this.user = null;
        this.comments = [];
        this.error = '';
    }

    async init() {

        this.bindEvents();

        const urlParams = new URLSearchParams(window.location.search);
        const postId = urlParams.get('id');

        try {
            const { id, title, body, userId } = await getPost(postId);
            this.post = new PostItem(id, title, body, userId);
            this.user = await getUser(this.post.userId);
            this.render();
        } catch (e) {
            this.error = e.message;
        }
    }

    bindEvents() {
        this.elBtnLoadComments.addEventListener('click', () => this.getPostComments());
    }

    async getPostComments() {

        try {
            this.comments = (await getCommentsForPost(this.post.id)).map(c => {
                return new Comment(c.id, c.name, c.body, c.postId)
            });
            this.render();
        } catch (error) {
            this.error = error.message;
        }
    }

    render() {
        console.log('rendering....');
        // Reset!
        this.elPostContent.innerHTML = '';
        this.elComments.innerHTML = '';
        this.elBtnLoadComments.disabled = false;

        // Post
        this.elPostContent.appendChild(this.post.render(false));

        // User
        this.elUser.innerText = `Posted by: ${this.user.username}`;

        // Comments
        this.elBtnLoadComments.disabled = false;

        console.log('comments: ', this.comments);

        for (const comment of this.comments) {
            console.log(comment);
            this.elComments.appendChild( comment.render() );
        }

    }
}

new PostDetail().init();



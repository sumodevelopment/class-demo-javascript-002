import { getPosts } from './api.js';
import PostListItem from './models/post-list-item.js';

class App {
    constructor() {
        this.elPosts = document.getElementById('posts');
        this.elSearchBox = document.getElementById('search');
        this.searchTerm = '';
        this.posts = [];
        this.error = '';
    }

    async init() {

        this.bindEvents();

        try {
            this.posts = (await getPosts()).map(p => new PostListItem(p.id, p.title,p.body, p.userId))
        } catch (e) {
            this.error = e.message;
        } finally {
            this.render();
        }
    }

    bindEvents() {
        // When the Clear button is pressed.
        this.elSearchBox.addEventListener('search', this.onSearchChanged.bind(this));
        // When typing occurs.
        this.elSearchBox.addEventListener('keyup', this.onSearchChanged.bind(this));
    }

    onSearchChanged(event) {
        this.searchTerm = event.target.value.trim(); 
        this.render();
    }

    render() {

        if (this.error) {
            // Render an error!!
            
            return;
        }
    
        this.elPosts.innerHTML = '';
        this.posts.filter(post => {
            return post.title.toLowerCase().includes( this.searchTerm.toLowerCase() );
        }).forEach(post => {
            this.elPosts.appendChild( post.render() );
        });
    
    }
    
}

new App().init();
# Class Demo 2

Using Async/Await in JavaScript with the Placeholder API from typicode.

### Topics
* Promises using Fetch API
* Functional constructors

### Resources
API provided by typicode: [https://jsonplaceholder.typicode.com/](https://jsonplaceholder.typicode.com/)

